mod core;

use bot_common::core::types::constants::strings::MSG_START;
use lazy_static::lazy_static;
use teloxide::dispatching::dialogue::{self, InMemStorage};
use teloxide::dispatching::UpdateHandler;
use teloxide::prelude::*;

use crate::core::app::bot_functions::*;
use crate::core::types::teloxide::{Command, State};
use bot_common::core::app::utils::get_exec_data;
use bot_common::core::types::teloxide::ExecData;

lazy_static! {
    static ref EXEC_DATA: ExecData = get_exec_data();
}

#[tokio::main]
async fn main() {
    let bot = Bot::from_env();

    // Welcome message
    let _ = bot.send_message(EXEC_DATA.client_id, MSG_START).await;

    // Command::repl(bot, answer).await;
    Dispatcher::builder(bot, schema())
        .dependencies(dptree::deps![InMemStorage::<State>::new()])
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;
}

fn schema() -> UpdateHandler<Box<dyn std::error::Error + Send + Sync + 'static>> {
    use dptree::case;

    // fsm setup based in: https://trkohler.com/posts/i-migrated-my-family-finance-bot-from-python-to-rust-because-i-am-tired-of-exceptions/

    let command_handler = teloxide::filter_command::<Command, _>()
        .branch(
            case![State::Start]
                .branch(case![Command::Help].endpoint(help))
                .branch(case![Command::About].endpoint(about))
                .branch(case![Command::Hello].endpoint(hello))
                .branch(case![Command::OS].endpoint(os))
                .branch(case![Command::Ip].endpoint(ip))
                .branch(case![Command::Uptime].endpoint(uptime))
                .branch(case![Command::Wol(mac_address)].endpoint(wol))
                .branch(case![Command::Ping(host)].endpoint(ping))
                .branch(case![Command::Shutdown].endpoint(make_confirmation))
                .branch(case![Command::Reboot].endpoint(make_confirmation)),
        )
        .branch(case![Command::Cancel].endpoint(cancel));

    let message_handler = Update::filter_message()
        .branch(command_handler)
        .branch(
            case![State::ReceiveConfirmationShutdown { response }]
                .endpoint(receive_confirmation_shutdown),
        )
        .branch(
            case![State::ReceiveConfirmationReboot { response }]
                .endpoint(receive_confirmation_reboot),
        )
        .branch(dptree::endpoint(invalid_state));

    dialogue::enter::<Update, InMemStorage<State>, State, _>().branch(message_handler)
}
