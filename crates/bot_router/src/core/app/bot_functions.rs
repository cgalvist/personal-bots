//! Bot functions (Teloxide).

use teloxide::{
    payloads::SendMessageSetters,
    requests::Requester,
    types::{KeyboardButton, KeyboardMarkup, KeyboardRemove, Message, ReplyParameters},
    utils::command::BotCommands,
    Bot,
};

use crate::{
    core::types::{
        constants::strings::{MSG_PING_HELP, MSG_WOL, MSG_WOL_HELP},
        teloxide::{Command, CustomDialogue, HandlerResult, State},
    },
    EXEC_DATA,
};

use bot_common::core::{
    app::utils::{cast_bool_response, is_valid_msg, run_action_typing},
    types::constants::strings::{
        MSG_ABOUT, MSG_CANCEL, MSG_CANCEL_DIAG, MSG_CONFIRM, MSG_FINISHED, MSG_HELLO,
        MSG_INVALID_STATE, MSG_PROC_FAILED, MSG_REBOOT, MSG_SHUTDOWN, OPT_NO, OPT_YES,
    },
};

use std::process::Command as OsCommand;

/// Returns bot functions
pub async fn help(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, Command::descriptions().to_string())
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
        dialogue.update(State::Start).await?;
    }

    Ok(())
}

/// Returns text about this project
pub async fn about(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, MSG_ABOUT)
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
        dialogue.update(State::Start).await?;
    }

    Ok(())
}

/// Returns an echo message
pub async fn hello(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, MSG_HELLO)
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
        dialogue.update(State::Start).await?;
    }

    Ok(())
}

/// Get Operative System data
pub async fn os(bot: Bot, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        match OsCommand::new("cat").arg("/etc/openwrt_release").output() {
            Ok(output) => {
                let output_str = String::from_utf8_lossy(&output.stdout);
                bot.send_message(msg.chat.id, output_str)
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?
            }
            Err(_error) => {
                bot.send_message(msg.chat.id, MSG_PROC_FAILED)
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?
            }
        };
    }

    Ok(())
}

/// Get WAN IP address
pub async fn ip(bot: Bot, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        run_action_typing(&bot, &msg.chat.id).await;

        match OsCommand::new("curl").arg("ifconfig.me").output() {
            Ok(output) => {
                let output_str = String::from_utf8_lossy(&output.stdout);
                bot.send_message(msg.chat.id, format!("🌎 {}", output_str))
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?
            }
            Err(_error) => {
                bot.send_message(msg.chat.id, MSG_PROC_FAILED)
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?
            }
        };
    }

    Ok(())
}

/// Get Uptime
pub async fn uptime(bot: Bot, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        match OsCommand::new("uptime").arg("").output() {
            Ok(output) => {
                let output_str = String::from_utf8_lossy(&output.stdout);
                bot.send_message(msg.chat.id, format!("⏰\n{}", output_str))
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?
            }
            Err(_error) => {
                bot.send_message(msg.chat.id, MSG_PROC_FAILED)
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?
            }
        };
    }

    Ok(())
}

/// Ping
pub async fn ping(bot: Bot, msg: Message, host: String) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if host.is_empty() {
            bot.send_message(msg.chat.id, MSG_PING_HELP)
                .reply_parameters(ReplyParameters::new(msg.id))
                .await?;
        } else {
            run_action_typing(&bot, &msg.chat.id).await;

            match OsCommand::new("ping").arg(host).arg("-w").arg("1").output() {
                Ok(output) => {
                    let output_str = String::from_utf8_lossy(&output.stdout);
                    bot.send_message(msg.chat.id, output_str)
                        .reply_parameters(ReplyParameters::new(msg.id))
                        .await?
                }
                Err(_error) => {
                    bot.send_message(msg.chat.id, MSG_PROC_FAILED)
                        .reply_parameters(ReplyParameters::new(msg.id))
                        .await?
                }
            };
        }
    }

    Ok(())
}

/// Wake on Lan
pub async fn wol(bot: Bot, msg: Message, mac_address: String) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if mac_address.is_empty() {
            bot.send_message(msg.chat.id, MSG_WOL_HELP)
                .reply_parameters(ReplyParameters::new(msg.id))
                .await?;
        } else {
            run_action_typing(&bot, &msg.chat.id).await;

            match OsCommand::new("etherwake")
                .arg("-i")
                .arg("br-lan")
                .arg(&mac_address)
                .output()
            {
                Ok(_) => {
                    bot.send_message(msg.chat.id, MSG_WOL)
                        .reply_parameters(ReplyParameters::new(msg.id))
                        .await?
                }
                Err(_error) => {
                    bot.send_message(msg.chat.id, MSG_PROC_FAILED)
                        .reply_parameters(ReplyParameters::new(msg.id))
                        .await?
                }
            };
        }
    }

    Ok(())
}

/// Cancels current user dialogue
pub async fn cancel(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, MSG_CANCEL_DIAG)
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
        dialogue.exit().await?;
    }

    Ok(())
}

/// Check invalid state in FSM
pub async fn invalid_state(bot: Bot, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, MSG_INVALID_STATE)
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
    }

    Ok(())
}

/// Generate user confirmation
pub async fn make_confirmation(
    bot: Bot,
    dialogue: CustomDialogue,
    msg: Message,
    cmd: Command,
) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if let Some(response_str) = msg.text().map(ToOwned::to_owned) {
            let boolean_options = [OPT_YES, OPT_NO].map(|option| KeyboardButton::new(option));

            bot.send_message(msg.chat.id, MSG_CONFIRM)
                .reply_markup(KeyboardMarkup::new([boolean_options]).one_time_keyboard())
                .reply_parameters(ReplyParameters::new(msg.id))
                .await?;

            let response = cast_bool_response(&response_str);

            match cmd {
                Command::Shutdown => {
                    dialogue
                        .update(State::ReceiveConfirmationShutdown { response })
                        .await?
                }
                Command::Reboot => {
                    dialogue
                        .update(State::ReceiveConfirmationReboot { response })
                        .await?
                }
                _ => dialogue.exit().await?,
            }
        }
    }

    Ok(())
}

/// Processes the confirmation to turn off the device
pub async fn receive_confirmation_shutdown(
    bot: Bot,
    dialogue: CustomDialogue,
    msg: Message,
) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if let Some(response_str) = msg.text().map(ToOwned::to_owned) {
            let response = cast_bool_response(&response_str);

            if response {
                bot.send_message(dialogue.chat_id(), MSG_SHUTDOWN)
                    .reply_markup(KeyboardRemove::new())
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?;

                match OsCommand::new("poweroff").arg("-d").arg("15").output() {
                    Ok(_) => {
                        bot.send_message(dialogue.chat_id(), MSG_FINISHED)
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                    Err(_error) => {
                        bot.send_message(dialogue.chat_id(), MSG_PROC_FAILED)
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                };
            } else {
                bot.send_message(dialogue.chat_id(), MSG_CANCEL)
                    .reply_markup(KeyboardRemove::new())
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?;
            }

            dialogue.update(State::Start).await?;
        }
    }

    Ok(())
}

/// Processes the confirmation to restart the device
pub async fn receive_confirmation_reboot(
    bot: Bot,
    dialogue: CustomDialogue,
    msg: Message,
) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if let Some(response_str) = msg.text().map(ToOwned::to_owned) {
            let response = cast_bool_response(&response_str);

            if response {
                bot.send_message(dialogue.chat_id(), MSG_REBOOT)
                    .reply_markup(KeyboardRemove::new())
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?;

                match OsCommand::new("reboot").arg("-d").arg("15").output() {
                    Ok(_) => {
                        bot.send_message(dialogue.chat_id(), MSG_FINISHED)
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                    Err(_error) => {
                        bot.send_message(dialogue.chat_id(), MSG_PROC_FAILED)
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                };
            } else {
                bot.send_message(dialogue.chat_id(), MSG_CANCEL)
                    .reply_markup(KeyboardRemove::new())
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?;
            }

            dialogue.update(State::Start).await?;
        }
    }

    Ok(())
}
