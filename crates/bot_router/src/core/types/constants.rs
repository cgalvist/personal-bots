//! General constants.

pub mod strings {
    pub const MSG_PING_HELP: &str = "Usage: /ping host";
    pub const MSG_WOL: &str = "Wake up device ⏰";
    pub const MSG_WOL_HELP: &str = "Usage: /wol mac_address";
}
