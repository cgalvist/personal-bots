//! Teloxide types.

use teloxide::{
    dispatching::dialogue::{Dialogue, InMemStorage},
    utils::command::BotCommands,
};

pub type CustomDialogue = Dialogue<State, InMemStorage<State>>;
pub type HandlerResult = Result<(), Box<dyn std::error::Error + Send + Sync>>;

/// Teloxide bot commands
#[derive(BotCommands, Clone, Debug)]
#[command(
    rename_rule = "lowercase",
    description = "These commands are supported:"
)]
pub enum Command {
    #[command(description = "Display this text.")]
    Help,
    #[command(description = "Send test message.")]
    Hello,
    #[command(description = "Get current operative system.")]
    OS,
    #[command(description = "Get WAN IP address.")]
    Ip,
    #[command(description = "Take screenshoot.")]
    Screenshoot,
    #[command(description = "Sleep system.")]
    Sleep,
    #[command(description = "Log out in current session.")]
    LogOut,
    #[command(description = "Shutdown system.")]
    Shutdown,
    #[command(description = "Reboot system.")]
    Reboot,
    #[command(description = "Cancel dialogue.")]
    Cancel,
    #[command(description = "About this bot.")]
    About,
}

/// Teloxide states for custom FSM
#[derive(Clone, Default)]
pub enum State {
    #[default]
    Start,
    ReceiveConfirmationSleep {
        response: bool,
    },
    ReceiveConfirmationLogOut {
        response: bool,
    },
    ReceiveConfirmationShutdown {
        response: bool,
    },
    ReceiveConfirmationReboot {
        response: bool,
    },
}
