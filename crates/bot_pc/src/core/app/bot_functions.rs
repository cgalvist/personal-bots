//! Bot functions (Teloxide).

use std::{env, fs, path::PathBuf};

use chrono::Utc;
use system_shutdown::{logout, reboot, shutdown, sleep};
use teloxide::{
    payloads::SendMessageSetters,
    requests::Requester,
    types::{InputFile, KeyboardButton, KeyboardMarkup, KeyboardRemove, Message, ReplyParameters},
    utils::command::BotCommands,
    Bot,
};
use xcap::Monitor;

use crate::{
    core::types::teloxide::{Command, CustomDialogue, HandlerResult, State},
    EXEC_DATA,
};

use std::process::Command as OsCommand;

use bot_common::core::{
    app::utils::{cast_bool_response, is_valid_msg, normalized, run_action_typing},
    types::constants::strings::{
        MSG_ABOUT, MSG_CANCEL, MSG_CANCEL_DIAG, MSG_CONFIRM, MSG_HELLO, MSG_INVALID_STATE,
        MSG_PROC_FAILED, MSG_REBOOT, MSG_SHUTDOWN, MSG_SIGN_OUT, MSG_SLEEP, OPT_NO, OPT_YES,
    },
};

/// Returns bot functions
pub async fn help(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, Command::descriptions().to_string())
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
        dialogue.update(State::Start).await?;
    }

    Ok(())
}

/// Returns text about this project
pub async fn about(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, MSG_ABOUT)
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
        dialogue.update(State::Start).await?;
    }

    Ok(())
}

/// Returns an echo message
pub async fn hello(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, MSG_HELLO)
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
        dialogue.update(State::Start).await?;
    }

    Ok(())
}

/// Returns current Operative System
pub async fn os(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, env::consts::OS)
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
        dialogue.update(State::Start).await?;
    }

    Ok(())
}

/// Get WAN IP address
pub async fn ip(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        run_action_typing(&bot, &msg.chat.id).await;

        match OsCommand::new("curl").arg("ifconfig.me").output() {
            Ok(output) => {
                let output_str = String::from_utf8_lossy(&output.stdout);
                bot.send_message(msg.chat.id, format!("🌎 {}", output_str))
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?
            }
            Err(_error) => {
                bot.send_message(msg.chat.id, MSG_PROC_FAILED)
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?
            }
        };

        dialogue.update(State::Start).await?;
    }

    Ok(())
}

/// Returns a screenshot
pub async fn screenshot(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        run_action_typing(&bot, &msg.chat.id).await;

        match Monitor::all() {
            Ok(monitors) => {
                if monitors.len() <= 0 {
                    bot.send_message(msg.chat.id, "No monitors detected")
                        .reply_parameters(ReplyParameters::new(msg.id))
                        .await?;
                    return Ok(());
                }

                let dir = env::temp_dir();

                for monitor in monitors {
                    let abs_path: String = format!(
                        "{}/monitor-{}-{}.png",
                        dir.to_string_lossy(),
                        normalized(monitor.name()),
                        Utc::now().timestamp()
                    );

                    let photo = PathBuf::from(&abs_path);

                    match monitor.capture_image() {
                        Ok(image) => {
                            match image.save(&abs_path) {
                                Ok(_) => {
                                    bot.send_photo(msg.chat.id, InputFile::file(photo)).await?;

                                    fs::remove_file(&abs_path)?
                                }
                                Err(error) => {
                                    log::error!(
                                        "{} ({}): {}",
                                        MSG_PROC_FAILED,
                                        "Save image",
                                        error
                                    );
                                    bot.send_message(dialogue.chat_id(), MSG_PROC_FAILED)
                                        .reply_parameters(ReplyParameters::new(msg.id))
                                        .await?;
                                }
                            };
                        }
                        Err(error) => {
                            log::error!("{} ({}): {}", MSG_PROC_FAILED, "Capture image", error);
                            bot.send_message(dialogue.chat_id(), MSG_PROC_FAILED)
                                .reply_parameters(ReplyParameters::new(msg.id))
                                .await?;
                        }
                    }
                }

                dialogue.update(State::Start).await?;
            }
            Err(error) => {
                log::error!("{} ({}): {}", MSG_PROC_FAILED, "Get monitors data", error);
                bot.send_message(dialogue.chat_id(), MSG_PROC_FAILED)
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?;
            }
        };
    }

    Ok(())
}

/// Cancels current user dialogue
pub async fn cancel(bot: Bot, dialogue: CustomDialogue, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, MSG_CANCEL_DIAG)
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
        dialogue.exit().await?;
    }

    Ok(())
}

/// Check invalid state in FSM
pub async fn invalid_state(bot: Bot, msg: Message) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        bot.send_message(msg.chat.id, MSG_INVALID_STATE)
            .reply_parameters(ReplyParameters::new(msg.id))
            .await?;
    }

    Ok(())
}

/// Generate user confirmation
pub async fn make_confirmation(
    bot: Bot,
    dialogue: CustomDialogue,
    msg: Message,
    cmd: Command,
) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if let Some(response_str) = msg.text().map(ToOwned::to_owned) {
            let boolean_options = [OPT_YES, OPT_NO].map(|option| KeyboardButton::new(option));

            bot.send_message(msg.chat.id, MSG_CONFIRM)
                .reply_markup(KeyboardMarkup::new([boolean_options]).one_time_keyboard())
                .reply_parameters(ReplyParameters::new(msg.id))
                .await?;

            let response = cast_bool_response(&response_str);

            match cmd {
                Command::Sleep => {
                    dialogue
                        .update(State::ReceiveConfirmationSleep { response })
                        .await?
                }
                Command::LogOut => {
                    dialogue
                        .update(State::ReceiveConfirmationLogOut { response })
                        .await?
                }
                Command::Shutdown => {
                    dialogue
                        .update(State::ReceiveConfirmationShutdown { response })
                        .await?
                }
                Command::Reboot => {
                    dialogue
                        .update(State::ReceiveConfirmationReboot { response })
                        .await?
                }
                _ => dialogue.exit().await?,
            }
        }
    }

    Ok(())
}

/// Processes the confirmation to suspend the session
pub async fn receive_confirmation_sleep(
    bot: Bot,
    dialogue: CustomDialogue,
    msg: Message,
) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if let Some(response_str) = msg.text().map(ToOwned::to_owned) {
            let response = cast_bool_response(&response_str);

            if response {
                match sleep() {
                    Ok(_) => {
                        bot.send_message(dialogue.chat_id(), MSG_SLEEP)
                            .reply_markup(KeyboardRemove::new())
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                    Err(error) => {
                        log::error!("{}: {}", MSG_PROC_FAILED, error);
                        bot.send_message(dialogue.chat_id(), MSG_PROC_FAILED)
                            .reply_markup(KeyboardRemove::new())
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                };
            } else {
                bot.send_message(dialogue.chat_id(), MSG_CANCEL)
                    .reply_markup(KeyboardRemove::new())
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?;
            }
        }

        dialogue.update(State::Start).await?;
    }

    Ok(())
}

/// Processes the confirmation to close the session
pub async fn receive_confirmation_logout(
    bot: Bot,
    dialogue: CustomDialogue,
    msg: Message,
) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if let Some(response_str) = msg.text().map(ToOwned::to_owned) {
            let response = cast_bool_response(&response_str);

            if response {
                match logout() {
                    Ok(_) => {
                        bot.send_message(dialogue.chat_id(), MSG_SIGN_OUT)
                            .reply_markup(KeyboardRemove::new())
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                    Err(error) => {
                        log::error!("{}: {}", MSG_PROC_FAILED, error);
                        bot.send_message(dialogue.chat_id(), MSG_PROC_FAILED)
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                };
            } else {
                bot.send_message(dialogue.chat_id(), MSG_CANCEL)
                    .reply_markup(KeyboardRemove::new())
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?;
            }

            dialogue.update(State::Start).await?;
        }
    }

    Ok(())
}

/// Processes the confirmation to turn off the computer
pub async fn receive_confirmation_shutdown(
    bot: Bot,
    dialogue: CustomDialogue,
    msg: Message,
) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if let Some(response_str) = msg.text().map(ToOwned::to_owned) {
            let response = cast_bool_response(&response_str);

            if response {
                match shutdown() {
                    Ok(_) => {
                        bot.send_message(dialogue.chat_id(), MSG_SHUTDOWN)
                            .reply_markup(KeyboardRemove::new())
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                    Err(error) => {
                        log::error!("{}: {}", MSG_PROC_FAILED, error);
                        bot.send_message(dialogue.chat_id(), MSG_PROC_FAILED)
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                };
            } else {
                bot.send_message(dialogue.chat_id(), MSG_CANCEL)
                    .reply_markup(KeyboardRemove::new())
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?;
            }

            dialogue.update(State::Start).await?;
        }
    }

    Ok(())
}

/// Processes the confirmation to restart the computer
pub async fn receive_confirmation_reboot(
    bot: Bot,
    dialogue: CustomDialogue,
    msg: Message,
) -> HandlerResult {
    if is_valid_msg(&bot, &msg, &EXEC_DATA).await {
        if let Some(response_str) = msg.text().map(ToOwned::to_owned) {
            let response = cast_bool_response(&response_str);

            if response {
                match reboot() {
                    Ok(_) => {
                        bot.send_message(dialogue.chat_id(), MSG_REBOOT)
                            .reply_markup(KeyboardRemove::new())
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                    Err(error) => {
                        log::error!("{}: {}", MSG_PROC_FAILED, error);
                        bot.send_message(dialogue.chat_id(), MSG_PROC_FAILED)
                            .reply_parameters(ReplyParameters::new(msg.id))
                            .await?
                    }
                };
            } else {
                bot.send_message(dialogue.chat_id(), MSG_CANCEL)
                    .reply_markup(KeyboardRemove::new())
                    .reply_parameters(ReplyParameters::new(msg.id))
                    .await?;
            }

            dialogue.update(State::Start).await?;
        }
    }

    Ok(())
}
