pub mod core;

#[cfg(test)]
mod tests {
    use crate::core::app::utils::{cast_bool_response, get_property_name, normalized};

    #[tokio::test]
    async fn test_get_property_name_some() {
        let property = Some("test_property");
        assert_eq!(get_property_name(property), "test_property");
    }

    #[tokio::test]
    async fn test_get_property_name_none() {
        let property = None;
        assert_eq!(get_property_name(property), "");
    }

    #[test]
    fn test_normalized() {
        let filename = "file|name\\with:illegal/characters.txt";
        assert_eq!(normalized(filename), "filenamewithillegalcharacters.txt");
    }

    #[test]
    fn test_cast_bool_response_yes() {
        assert_eq!(cast_bool_response("Yes"), true);
    }

    #[test]
    fn test_cast_bool_response_no() {
        assert_eq!(cast_bool_response("No"), false);
    }

    #[test]
    fn test_cast_bool_response_other() {
        assert_eq!(cast_bool_response("Other"), false);
    }
}
