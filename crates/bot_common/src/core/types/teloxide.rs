use chrono::{DateTime, Utc};
use teloxide::types::ChatId;

/// Custom execution data
pub struct ExecData {
    pub start_date: DateTime<Utc>,
    pub client_id: ChatId,
}
