//! General constants.

pub mod strings {
    pub const MSG_STARTING: &str = "Starting command bot...";
    pub const MSG_START: &str = "Process started 🤖";
    pub const MSG_HELLO: &str = "Hello! 😃";
    pub const MSG_ABOUT: &str = "Made with 💙 by cgalvist.";
    pub const MSG_CANCEL_DIAG: &str = "Cancelling dialogue";
    pub const MSG_INVALID_STATE: &str =
        "Unable to handle the message. Type /help to see the usage.";
    pub const MSG_CONFIRM: &str = "Are you sure?";
    pub const OPT_YES: &str = "Yes";
    pub const OPT_NO: &str = "No";
    pub const MSG_CANCEL: &str = "Canceled process ✅";
    pub const MSG_FINISHED: &str = "Finished process ✅";
    pub const MSG_PROC_FAILED: &str = "Process failed ⚠️";
    pub const MSG_SLEEP: &str = "Sleeping... 😴";
    pub const MSG_SIGN_OUT: &str = "Signing out... 👋";
    pub const MSG_SHUTDOWN: &str = "Shutting down... 😴";
    pub const MSG_REBOOT: &str = "Rebooting... 🌀";
}
