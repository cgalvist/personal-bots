//! General functions.

use teloxide::{
    requests::Requester,
    types::{ChatId, Message},
    Bot,
};

use crate::core::types::teloxide::ExecData;
use std::{env, process};

/// Returns property name as string
pub fn get_property_name(property: Option<&str>) -> &str {
    match property {
        Option::Some(value) => value,
        Option::None => "",
    }
}

/// Normalizes a path to a file or folder
pub fn normalized(filename: &str) -> String {
    filename
        .replace("|", "")
        .replace("\\", "")
        .replace(":", "")
        .replace("/", "")
}

/// Converts a string to a boolean variable
pub fn cast_bool_response(response_str: &str) -> bool {
    match response_str.as_ref() {
        "Yes" => true,
        "No" => false,
        _ => false,
    }
}

/// Checks if an user bot message is valid
pub async fn is_valid_msg(bot: &Bot, msg: &Message, exec_data: &ExecData) -> bool {
    if exec_data.start_date > msg.date {
        return false;
    }
    if exec_data.client_id != msg.chat.id {
        send_ext_user_msg(&bot, &msg, &exec_data.client_id).await;
        return false;
    }

    true
}

/// Run typing action
pub async fn run_action_typing(bot: &Bot, client_id: &ChatId) {
    _ = bot
        .send_chat_action(*client_id, teloxide::types::ChatAction::Typing)
        .await;
}

/// Send a message reporting unauthorized access
async fn send_ext_user_msg(bot: &Bot, msg: &Message, client_id: &ChatId) {
    _ = bot.send_message(
        *client_id,
        format!(
            "⚠️ Someone with username '{}' ('{}', '{}') is trying to use your bot, sending the command '{:?}'",
            get_property_name(msg.chat.username()),
            get_property_name(msg.chat.first_name()),
            get_property_name(msg.chat.last_name()),
            msg.kind
        ),
    )
    .await;
}

/// Get bot execution data
pub fn get_exec_data() -> ExecData {
    return ExecData {
        start_date: chrono::offset::Utc::now(),
        client_id: match env::var("TELOXIDE_CLIENT_ID") {
            Ok(chat_id_str) => match chat_id_str.parse::<i64>() {
                Ok(chat_id) => ChatId(chat_id),
                Err(_) => {
                    log::error!("Unable to parse client ID '{}'.", chat_id_str);
                    process::exit(1);
                }
            },
            Err(e) => {
                log::error!("Couldn't read chat id ({:?})", e);
                process::exit(1);
            }
        },
    };
}
