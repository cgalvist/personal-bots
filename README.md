# Personal bots

Private personal bots with Telegram.

Made in [RUST 🦀](https://www.rust-lang.org/es) with [Teloxide](https://github.com/teloxide/teloxide)

## Setting up your environment

1. Create a new bot using [@Botfather](https://t.me/botfather) to get a token in the format `123456789:blablabla`.

2. Check your client using [@IdBot](https://t.me/myidbot) to get a identifier in the format `12346789`.

### Initialise the environment variables

#### Unix-like

```sh
export TELOXIDE_TOKEN=<Add secret here>
export TELOXIDE_CLIENT_ID=<Add secret here>
```

#### Windows CMD

```bat
set TELOXIDE_TOKEN=<Add secret here>
set TELOXIDE_CLIENT_ID=<Add secret here>
```

#### Windows PowerShell

```ps1
$env:TELOXIDE_TOKEN=<Add secret here>
$env:TELOXIDE_CLIENT_ID=<Add secret here>
```

### Run a bot

```sh
# Run bot (desktop pc)
cargo run -p bot_pc
# Run bot (server)
cargo run -p bot_server
# Run bot (router)
cargo run -p bot_router
```

### Useful commands

```sh
# Test common library
cargo test -p bot_common
# Build release version
cargo build --release
```

## Install

sudo apt-get install pkg-config libssl-dev

### Router bot (OpenWRT)

Compile project

```sh
# Check processor info (in openwrt router)
cat /proc/cpuinfo
# Check rust targets
rustc --print target-list | grep mips
```

#### Compilation steps

##### Example: ASUS RT-N16 router with MIPSel

```sh
# Build Docker image
docker build --tag bot-router-mipsel --file resources/docker/Mipsel.Dockerfile .
# Compile project
docker run --rm -v "$PWD":/usr/src/rust-app -w /usr/src/rust-app bot-router-mipsel resources/scripts/cross-compile.sh
# Enter to interactive mode (optional)
docker run -it --rm -v "$PWD":/usr/src/rust-app -w /usr/src/rust-app bot-router-mipsel /bin/bash
```

#### Installation

Send files to router

```sh
# Send binary to router
scp -O target/mipsel-unknown-linux-musl/release/bot_router root@openwrt.lan:/usr/bin/
# Send service script to router
scp -O resources/scripts/bot_router root@openwrt.lan:/etc/init.d/
```

Save API keys

```sh
# Save API keys
vim /etc/config/bot_router
```

```sh
TELOXIDE_TOKEN="[ADD_SECRET_HERE]"
TELOXIDE_CLIENT_ID="[ADD_SECRET_HERE]"
```

```sh
# Add restricted permissions
chmod 600 /etc/config/bot_router
```

Install dependencies and setup service

```sh
# Install dependencies
opkg update
opkg install libatomic curl
# Enable service
/etc/init.d/bot_router enable
# Start service
service bot_router start
```

### PC or Server bot (Ubuntu 24.04)

Install dependencies (only required for PC bot)

```sh
sudo apt update
sudo apt -y install libxcb1 libxrandr2 libdbus-1-3
```

Install bot

```sh
# Install bot binary
cargo build --release -p $BOT_NAME
sudo mkdir -p /opt/TelegramBot/
sudo cp target/release/$BOT_NAME /opt/TelegramBot/telegrambot
sudo chmod +x /opt/TelegramBot/telegrambot
# Install bot service
sudo cp resources/ubuntu/systemd/telegram-bot.service /lib/systemd/system/
sudo chmod 644 /lib/systemd/system/telegram-bot.service
```

Add secrets in the file `/lib/systemd/system/telegram-bot.service`, after the previous `[Service]` parameters:

```conf
[Service]
...
Environment="TELOXIDE_TOKEN=[ADD_SECRET_HERE]"
Environment="TELOXIDE_CLIENT_ID=[ADD_SECRET_HERE]"
```

Enable the service:

```sh
# Enabling service
sudo systemctl daemon-reload
sudo systemctl enable telegram-bot --now
# Check logs
journalctl -xeu telegram-bot
```