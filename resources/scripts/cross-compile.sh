#!/bin/bash

###################################################
# Script for cross compile and binary compression #
###################################################

EXEC_PATH=/usr/src/rust-app

cd $EXEC_PATH
echo "Cross compile project ..."
cargo build -Zbuild-std=std,panic_abort --package $PROJECT_NAME --release --target $PROJECT_TARGET
echo "Compress binary ..."
/usr/upx/upx --best --lzma $EXEC_PATH/target/$PROJECT_TARGET/release/$PROJECT_NAME

exit 0