# Dockerfile build example for ASUS RT-N16 router (mipsel processor)
## Code based in: 
### https://github.com/japaric-archived/rust-on-openwrt
### https://medium.com/althea-mesh/cross-compiling-complex-rust-programs-for-openwrt-targets-80897fcf7648
### https://blog.dend.ro/building-rust-for-routers/

# Build stage
FROM rust:1.78.0

# Install OpenWRT Toolchain
## Check your router data (release, data and architecture) with "cat /etc/openwrt_release" command
## format: https://downloads.openwrt.org/releases/$RELEASE/targets/$TARGET/openwrt-toolchain-${RELEASE}-${DRIVER}-${ARCH}_gcc-${$GCC_version}.Linux-x86_64.tar.xz
WORKDIR /usr/openwrt-toolchain
RUN wget https://downloads.openwrt.org/releases/23.05.3/targets/bcm47xx/mips74k/openwrt-toolchain-23.05.3-bcm47xx-mips74k_gcc-12.3.0_musl.Linux-x86_64.tar.xz -O openwrt-toolchain.tar.xz
RUN tar -xf openwrt-toolchain.tar.xz
RUN mv /usr/openwrt-toolchain/*/* /usr/openwrt-toolchain
RUN rm openwrt-toolchain.tar.xz

# Install UPX
WORKDIR /usr/upx
RUN wget https://github.com/upx/upx/releases/download/v4.2.4/upx-4.2.4-amd64_linux.tar.xz -O upx.tar.xz
RUN tar -xf upx.tar.xz
RUN mv /usr/upx/*/* /usr/upx
RUN rm upx.tar.xz

# Rust dependencies
## Setup nightly version as default
RUN rustup default nightly
## Get nightly toolchain
RUN rustup component add rust-src --toolchain nightly-x86_64-unknown-linux-gnu

# Add environment variables
ENV EXEC_PATH=/usr/src/rust-app
ENV PROJECT_NAME=bot_router
ENV PROJECT_TARGET=mipsel-unknown-linux-musl
## Debug variables
ENV RUST_BACKTRACE=1
ENV CARGO_PROFILE_RELEASE_BUILD_OVERRIDE_DEBUG=true
## OpenWRT Toolchain variables
ENV OWRT_TOOLCHAIN="/usr/openwrt-toolchain"
ENV PATH="$PWD/$(echo $OWRT_TOOLCHAIN/toolchain-*/bin):$PATH"
## Rust variables
ENV CARGO_TARGET_MIPSEL_UNKNOWN_LINUX_MUSL_LINKER=$OWRT_TOOLCHAIN/toolchain-mipsel_74kc_gcc-12.3.0_musl/bin/mipsel-openwrt-linux-gcc
# ENV TARGET_CC=$OWRT_TOOLCHAIN/toolchain-mipsel_74kc_gcc-12.3.0_musl/bin/mipsel-openwrt-linux-gcc
# ENV HOST_CC=gcc
# ENV PKG_CONFIG_ALLOW_CROSS=1

# Copy source code
WORKDIR $EXEC_PATH
ADD Cargo.toml .
ADD /crates ./crates/
ADD resources/scripts/cross-compile.sh .

# Compile and compress project
RUN ./cross-compile.sh